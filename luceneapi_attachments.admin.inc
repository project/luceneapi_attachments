<?php

/**
 * @file
 *   Provides a file attachment search implementation for use with the Search Lucene module
 */

function luceneapi_attachments_admin_page() {
  $output = '';
  $output .= drupal_get_form('luceneapi_attachments_settings');
  $output .= drupal_get_form('luceneapi_attachments_delete_index_form');
  return $output;
}

/**
 * Displays the Attachment Settings Form.
*/
function luceneapi_attachments_settings() {
  $form = array();
  $default = implode(' ', luceneapi_attachments_default_excluded());
  $form['luceneapi_attachments_excluded_extensions'] = array(
    '#type' => 'textfield',
    '#title' => t('Excluded file extensions'),
    '#default_value' => variable_get('luceneapi_attachments_excluded_extensions', $default),
    '#size' => 80,
    '#maxlength' => 255,
    '#description' => t('File extensions that are excluded from indexing. Separate extensions with a space and do not include the leading dot. Extensions are internally mapped to a MIME type, so it is not necessary to put variations that map to the same type (e.g. tif is sufficient for tif and tiff)'),
  );
  $form['luceneapi_attachments_exclude_types'] = array(
    '#type' => 'radios',
    '#title' => t('Exclude files attached to a node of a type excluded by Search Lucene'),
    '#options' => array('0' => t('No'), '1' => t('Yes')),
    '#default_value' => variable_get('luceneapi_attachments_exclude_types', 1),
  );

  $form['luceneapi_attachments-cron-settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cron settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['luceneapi_attachments-cron-settings']['luceneapi_attachments_cron_limit'] = array(
    '#type' => 'select',
    '#title' => t('Maximum number of nodes to examine'),
    '#default_value' => variable_get('luceneapi_attachments_cron_limit', 100),
    '#options' => drupal_map_assoc(array(10, 20, 50, 100, 200)),
  );
  $form['luceneapi_attachments-cron-settings']['luceneapi_attachements_cron_time_limit'] = array(
    '#type' => 'select',
    '#title' => t('Maximum time to expend (sec)'),
    '#default_value' => variable_get('luceneapi_attachements_cron_time_limit', 15),
    '#options' => drupal_map_assoc(array(5, 10, 15, 20, 25, 30, 45, 60)),
  );
  $form['#submit'][] = 'luceneapi_attachments_settings_submit';
  return system_settings_form($form);
}

function luceneapi_attachments_settings_submit($form, &$form_state) {
  // Delete this so it's rebuilt.
  variable_del('luceneapi_attachments_excluded_mime');
  drupal_set_message(t('If you changed the allowed file extensions, you may need to delete and re-index all attachements.'));
}

/**
 * Create a form for deleting the contents of the Solr index.
 */
function luceneapi_attachments_delete_index_form() {
  $form = array();
  $form['markup'] = array(
    '#prefix' => '<h3>',
    '#value' => t('Index and cache controls'),
    '#suffix' => '</h3>',
  );
  $form['reindex'] = array(
    '#type' => 'submit',
    '#value' => t('Re-index all files'),
    '#submit' => array('luceneapi_attachments_reindex_submit'),
  );
  $form['reindex-desc'] = array(
    '#type' => 'item',
    '#description' => t('Re-indexing will add all file text to the index again (overwriting the index), but existing text in the index will remain searchable.'),
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete files from index'),
    '#submit' => array('luceneapi_attachments_delete_index_submit'),
  );
  $form['delete-desc'] = array(
    '#type' => 'item',
    '#description' => t('Deletes all of the files in the Solr index and reindexes them.  This may be needed if you have changed the allowed file extensions,if your index is corrupt, or if you have installed a new schema.xml.'),
  );
  $form['clear-cache'] = array(
    '#type' => 'submit',
    '#value' => t('Delete cached file text'),
    '#submit' => array('luceneapi_attachments_delete_cache_submit'),
  );
  $form['cache-desc'] = array(
    '#type' => 'item',
    '#description' => t('Deletes the local cache of extacted text from files. This will cause slower performance when reindexing ince text must be re-extracted.'),
  );
  return $form;
}

/**
 * Submit function for the "Re-index all content" button
 *
 */
function luceneapi_attachments_reindex_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/settings/luceneapi_node/attachments/confirm/reindex';
}

function luceneapi_attachments_delete_index_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/settings/luceneapi_node/attachments/confirm/delete';
}

function luceneapi_attachments_delete_cache_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/settings/luceneapi_node/attachments/confirm/clear-cache';
}

function luceneapi_attachments_confirm($form_state, $operation) {
  $form = array();
  $form['operation'] = array(
    '#type' => 'value',
    '#value' => $operation,
  );
  switch ($operation) {
    case 'reindex':
      $text = t('Are you sure you want to re-index the text of all file attachments?');
      break;
    case 'delete':
      $text = t('Are you sure you want to delete and re-index the text of all file attachments?');
      break;
    case 'clear-cache':
      $text = t('Are you sure you want to delete the cache of extracted text from file attachments?');
      break;
  }
  return confirm_form($form, $text, 'admin/settings/luceneapi_node/attachments/attachments', NULL, t('Confirm'), t('Cancel'));
}

function luceneapi_attachments_confirm_submit($form, &$form_state) {
  switch ($form_state['values']['operation']) {
    case 'delete':
      if (luceneapi_attachments_delete_index()) {
        drupal_set_message(t('File text has been deleted from the Apache Solr index. You must now !run_cron until all files have been re-indexed.', array('!run_cron' => l(t('run cron'), 'admin/reports/status/run-cron', array('query' => array('destination' => 'admin/settings/luceneapi_node'))))));
      }
      else {
        drupal_set_message(t('An error occurred'));
      }
      break;
    case 'reindex':
      // Mark all nodes as needing to be re-indexed
      variable_set('luceneapi_node:last_nid', 0);
      variable_set('luceneapi_node:last_change', 0);
      t('All file attachments will be re-indexed.');
      break;
    case 'clear-cache':
      db_query("DELETE FROM {luceneapi_attachments_files} WHERE removed = 0");
      drupal_set_message(t('The local cache of extracted text has been deleted.'));
      break;
  }
  $form_state['redirect'] = 'admin/settings/luceneapi_node/attachments';
}

function luceneapi_attachments_delete_index() {
  //$solr->deleteByQuery("entity:file AND hash:". luceneapi_site_hash());
  try {
    if ($index = luceneapi_index_open('luceneapi_node', $errstr)) {
      module_load_include('inc', 'luceneapi_node', 'luceneapi_node.index');
      
      // instantiates query container, adds subquery from hooks
      if ($query = luceneapi_query_get('boolean')) {
        if (!$id instanceof Zend_Search_Lucene_Search_Query) {
          if (!$id = luceneapi_query_get('term', $id, $field)) {
            throw new LuceneAPI_Exception(t('Error instantiating term query.'));
          }
        }
        luceneapi_subquery_add($query, $id, 'required', TRUE);
        foreach ($results as $subquery) {
          if ($subquery instanceof Zend_Search_Lucene_Search_Query) {
            luceneapi_subquery_add($query, $subquery, 'required', TRUE);
          }
        }
      }
      else {
        throw new LuceneAPI_Exception(t('Error instantiating boolean query.'));
      }

      // calls find() directly, results shouldn't be read from cache
      $doc_ids = array();
      $hits = $index->find($query);
      foreach ($hits as $hit) {
        $doc_ids[] = $hit->id;
      }
    }

    // deletes matching documents, commits changes to the database
    foreach ($doc_ids as $id) {
      $index->delete($id);
    }
    
    // returns TRUE if there were matched documents
    return !empty($doc_ids);
  }
  catch (Exception $e) {
    luceneapi_handle_error($e, $throw_exceptions);
  }
  return FALSE;
}

/**
 * Indexing-related functions
 */

/**
 * Index attachments
 *
 * Adds a document for each indexable file attachment for the given node ID.
 */
function luceneapi_attachments_add_documents($node, $namespace) {
  $document = FALSE;
  $hash = luceneapi_site_hash();
  // Since there is no notification for an attachment being unassociated with a
  // node (but that action will trigger it to be indexed again), we check for
  // fids that were added before but no longer present on this node.

  $fids = array();
  $failed = array();
  $result = db_query("SELECT fid, failed FROM {luceneapi_attachments_files} WHERE nid = %d", $node->nid);
  while ($row = db_fetch_array($result)) {
    $fids[$row['fid']] = $row['fid'];
    if($row['failed']) {
      $failed[$row['fid']] = $row['fid'];
    }
  }

  $files = luceneapi_attachments_get_indexable_files($node);
  // Find deleted files.
  $missing_fids = array_diff_key($fids, $files);
  if ($missing_fids) {
    db_query("UPDATE {luceneapi_attachments_files} SET removed = 1 WHERE fid IN (". db_placeholders($missing_fids) .")", $missing_fids);
  }
  $new_files = array_diff_key($files, $fids);
  // Filter out the files that have failed
  $new_files = array_diff_key($new_files, $failed);
  // Add new files.
  foreach ($new_files as $file) {
    db_query("INSERT INTO {luceneapi_attachments_files} (fid, nid, removed, sha1) VALUES (%d, %d, 0, '')", $file->fid, $node->nid);
  }
  foreach ($files as $file) {
    $text = luceneapi_attachments_get_attachment_text($file);
    if ($text) {
      // initializes document object
      $document = luceneapi_document_get();
      
      luceneapi_field_add($document, 'unstored', 'contents', luceneapi_html_prepare($file->description.'  '.$text), TRUE);
      
      // strips tags from title, adds title to document
      $title = luceneapi_html_prepare($file->filename);
      luceneapi_field_add($document, 'unstored', 'title', $title, TRUE);
      luceneapi_field_add($document, 'keyword', 'title_sort', drupal_strtolower($title), TRUE);
  
      
      // creates keyword fields matching node properties
      $keywords = array(
        'changed' => gmdate('Y-m-d\TH:i:s\Z', $file->timestamp), 
        'created' => gmdate('Y-m-d\TH:i:s\Z', $file->timestamp), 
        'moderate', 
        'name' => $node->name, 
        'nid' => $node->nid, 
        'promote' => $node->promote, 
        'sticky' => $node->sticky,
        'type' => 'file', 
        'uid' => $node->uid,
        'site' => url(NULL, array('absolute' => TRUE)),
        'url' => file_create_url($file->filepath),
        'path' => $file->filepath,
        'hash' => $hash,
        'id' => '/file/'.$file->fid .'-'. $node->nid,
      );
    
      foreach ($keywords as $field => $keyword) {
        $keyword = drupal_strtolower((string)$keyword);
        luceneapi_field_add($document, 'keyword', $field, $keyword, TRUE);
      }
  
      
      luceneapi_field_add($document, 'keyword', 'ss_filemime', $file->filemime, TRUE);
      luceneapi_field_add($document, 'keyword', 'ss_file_node_title', luceneapi_strip_tags($node->title), TRUE);
      luceneapi_field_add($document, 'keyword', 'ss_file_node_url', url('node/' . $node->nid, array('absolute' => TRUE)), TRUE);
      
      // indexes taxonomy term IDs and names
      if (module_exists('taxonomy')) {
    
        // gets taxonomy information
        $vids = array();
        foreach (taxonomy_get_vocabularies() as $vid => $voc) {
          $vids[$vid] = array();
        }
        $tids = array();
        $tnames = array();
        if(!empty($node->taxonomy)) {
          foreach ($node->taxonomy as $taxonomy) {
            $tids[] = $taxonomy->tid;
            $tnames[] = $taxonomy->name;
            $vids[$taxonomy->vid][] = $taxonomy->tid;
          }
        }
    
        // adds term ids to the index, builds unindexed field for facet calculations
        if(!empty($tids)) {
          luceneapi_field_add($document, 'unstored', 'category', join(' ', $tids), TRUE);
        }
        if(!empty($vids)) {
          foreach ($vids as $vid => $vtids) {
            luceneapi_field_add($document, 'unindexed', 'category_'. $vid, join(' ', $vtids), TRUE);
          }
        }
        if(!empty($tnames)) {
          // adds taxonomy term names to the index
          luceneapi_field_add($document, 'unstored', 'taxonomy', join(' ', $tnames), TRUE);
        }
      }
    
      // adds nodeaccess grants as nodeaccess_ fields
      if (count(module_implements('node_grants'))) {
    
        // gets node access grants
        $sql = 'SELECT realm, gid'
             .' FROM {node_access}'
             .' WHERE nid = %d AND grant_view = 1';
        $result = db_query($sql, $node->nid);
    
        //builds array of fields to values
        $grants = array();
        while ($grant = db_fetch_object($result)) {
          $field = sprintf('nodeaccess_%s', $grant->realm);
          $grants[$field][] = $grant->gid;
        }
    
        // appends grants to the corresponding nodeaccess field
        foreach ($grants as $field => $values) {
          luceneapi_field_add($document, 'text', $field, join(' ', $values), TRUE);
        }
      }
      
      if (module_exists('luceneapi_og')) {
        // Index group posts
        if (!empty($node->og_groups)) {
          $group_nids = join(':', array_keys($node->og_groups));
          luceneapi_field_add($document, 'keyword', luceneapi_og_gid_key(), $group_nids);           
        }
        elseif (isset($node->og_description) && variable_get('luceneapi_og_groupnode', 0)) {
          // Index the group node itself as in the group.
          luceneapi_field_add($document, 'keyword', luceneapi_og_gid_key(), $node->nid);
        }
      }
      
      // Allow other modules to modify this document
      drupal_alter('luceneapi_attachment_index', $document, $node, $file);
      
      if (!$index = luceneapi_index_open('luceneapi_node', $errstr)) {
        throw new LuceneAPI_Exception($errstr);
      }
      $result = luceneapi_document_add($index, $document, $file, TRUE);
      luceneapi_index_commit($index, TRUE);
    }
    else {
      watchdog('luceneapi_attachments', t('Could not extract any indexable text from %filepath'), array('%filepath' => $file->filepath), WATCHDOG_WARNING);
    }
  }
}

/**
 * Return all non-excluded file attachments for a particular node
 */
function luceneapi_attachments_get_indexable_files($node) {
  $files = array();

  if(!empty($node->files)) {
    $files = $node->files;
  }

  $fields = luceneapi_attachments_get_cck_file_fields();
  foreach ($fields as $field) {
    if(!empty($node->$field)) {
      $files = array_merge($files, $node->$field);
    }
  }
  $file_list = array();
  foreach ($files as $file) {
    // Some are arrays others are objects, treat them all as objects
    $file = (object) $file;
    // Some filefield-enabled nodes show up with an emtpy file array.
    if (!empty($file->fid) && luceneapi_attachments_allowed_mime($file->filemime)) {
      if (isset($file->data['description']) && !isset($file->description)) {
        $file->description = $file->data['description'];
      }
      $file_list[$file->fid] = $file;
    }
  }
  return $file_list;
}

function luceneapi_attachments_default_excluded() {
  $default = array('aif', 'art', 'avi', 'bmp', 'gif', 'ico', 'jpg', 'mov', 'mp3', 'mp4', 'mpg', 'oga', 'ogv', 'png', 'psd', 'ra', 'ram', 'rgb', 'tif',);
  return $default;
}

function luceneapi_attachments_allowed_mime($filemime) {

  $excluded = variable_get('luceneapi_attachments_excluded_mime', FALSE);
  if ($excluded === FALSE) {
    // Build the list of excluded MIME types.
    $excluded = array();
    $extensions = variable_get('luceneapi_attachments_excluded_extensions', FALSE);
    if ($extensions !== FALSE) {
      $extensions = explode(' ', $extensions);
    }
    else {
      $extensions = luceneapi_attachments_default_excluded();
    }
    foreach ($extensions as $ext) {
      $ext = trim($ext);
      if ($ext) {
        $mime = file_get_mimetype('dummy.' . $ext);
        $excluded[$mime] = 1;
      }
    }
    variable_set('luceneapi_attachments_excluded_mime', $excluded);
  }
  return empty($excluded[$filemime]);
}

/**
 * Return all CCK fields that are of type 'file'
 */
function luceneapi_attachments_get_cck_file_fields() {
  $file_fields = array();
  if(module_exists('filefield')) {
    $fields = content_fields();
    foreach($fields as $key => $values){
      if($values['type'] == 'filefield') {
        $file_fields[] = $key;
      }
    }
  }
  return $file_fields;
}

/**
 * Parse the attachment getting just the raw text.
 *
 * @throws Exception
 */
function luceneapi_attachments_get_attachment_text($file) {
  // Any down-side to using realpath()?
  $filepath = realpath($file->filepath);
  // Check that we have a valid filepath.
  if (!($filepath) || !is_file($filepath)) {
    return FALSE;
  }

  // No need to use external apps for plain text files.
  if ($file->filemime == 'text/plain' || $file->filemime == 'text/x-diff') {
    $text = file_get_contents($filepath);
    // TODO - try to detect encoding and convert to UTF-8.
    // Strip bad control characters.
    $text = iconv("UTF-8", "UTF-8//IGNORE", $text);
    $text = trim(luceneapi_html_prepare($text));
    return $text;
  }

  $sha1 = sha1_file($filepath);
  if ($sha1 === FALSE) {
    return FALSE;
  }

  $cached = db_fetch_array(db_query("SELECT * FROM {luceneapi_attachments_files} WHERE fid = %d", $file->fid));

  if (!is_null($cached['body']) && ($cached['sha1'] == $sha1)) {
    // No need to re-extract.
    return $cached['body'];
  }

  $text = search_files_attachments_get_file_contents($file->filepath);
  
  // Strip bad control characters.
  $text = iconv("UTF-8", "UTF-8//IGNORE", $text);
  $text = trim(luceneapi_html_prepare($text));
  // Save the extracted, cleaned text to the DB.
  db_query("UPDATE {luceneapi_attachments_files} SET sha1 = '%s', body = '%s', attempts = 1, last_attempt = %d WHERE fid = %d", $sha1, $text, time(), $file->fid);

  return $text;
}

/**
 * Returns a site-specific hash.
 */
function luceneapi_site_hash() {
  if (!($hash = variable_get('luceneapi_site_hash', FALSE))) {
    global $base_url;
    // Set a random 6 digit base-36 number as the hash.
    $hash = substr(base_convert(sha1(uniqid($base_url, TRUE)), 16, 36), 0, 6);
    variable_set('luceneapi_site_hash', $hash);
  }
  return $hash;
}