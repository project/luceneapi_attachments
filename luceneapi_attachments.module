<?php

/**
 * @file
 *   Provides a file attachment search implementation for use with the Search Lucene module
 */

/**
 * Implementation of hook_menu().
 */
function luceneapi_attachments_menu() {
  $items = array();
  $items['admin/settings/luceneapi_node/attachments'] = array(
    'title' => t('File attachments'),
    'description' => 'Administer Search Lucene Attachments.',
    'page callback' => 'luceneapi_attachments_admin_page',
    'access arguments' => array('administer search'),
    'file' => 'luceneapi_attachments.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/settings/luceneapi_node/attachments/confirm/reindex'] = array(
    'title' => t('Reindex all files'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('luceneapi_attachments_confirm', 5),
    'access arguments' => array('administer search'),
    'file' => 'luceneapi_attachments.admin.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/settings/luceneapi_node/attachments/confirm/delete'] = array(
    'title' => t('Delete and reindex all files'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('luceneapi_attachments_confirm', 5),
    'access arguments' => array('administer search'),
    'file' => 'luceneapi_attachments.admin.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/settings/luceneapi_node/attachments/confirm/clear-cache'] = array(
    'title' => t('Delete the local cache of file text'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('luceneapi_attachments_confirm', 5),
    'access arguments' => array('administer search'),
    'file' => 'luceneapi_attachments.admin.inc',
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implementation of hook_help().
 */
function luceneapi_attachments_help($section) {
  switch ($section) {
    case 'admin/settings/luceneapi_node/statistics':
      $remaining = 0;
      $total = 0;
      // Collect the stats
      $status = luceneapi_attachments_search('status');
      $remaining += $status['remaining'];
      $total += $status['total'];
      return t('<br />There @items remaining to be examined for attachments out of @total total.', array(
        '@items' => format_plural($remaining, t('is 1 post'), t('are @count posts')),
        '@total' => $total,
      ));
      break;
  }
}

/**
 * Implementation of hook_search().
 */
function luceneapi_attachments_search($op = 'search', $keys = NULL) {

  switch ($op) {
    case 'name':
      return ''; // We dont want a tab
    case 'reset':
      variable_del('luceneapi_node:last_change');
      variable_del('luceneapi_node:last_nid');
      return;
    case 'status':
      $total = db_result(db_query(luceneapi_node_type_condition_add(
        'SELECT COUNT(*) FROM {node} n WHERE n.status = 1'
      )));

      $last_nid = variable_get('luceneapi_node:last_nid', 0);
      $last = variable_get('luceneapi_node:last_change', 0);
      $params = array($last, $last_nid, $last, $last, $last);

      // SQL that gets nodes that have changed since the last Lucene index update
      $sql = 'SELECT COUNT(*)'
           .' FROM {node} n'
           .' LEFT JOIN {node_comment_statistics} c ON n.nid = c.nid'
           .' WHERE n.status = 1'
           .'   AND ('
           .'     (GREATEST(n.changed, c.last_comment_timestamp) = %d AND n.nid > %d) OR'
           .'     (n.changed > %d OR c.last_comment_timestamp > %d)'
           .'   )';

      // adds statement to ignore excluded content types, gets num remaining
      $sql = luceneapi_node_type_condition_add($sql);
      $remaining = db_result(db_query($sql, $params));

      return array(
        'remaining' => $remaining,
        'total' => $total
      );
    case 'search':
      return array();
  }
}

/**
 * 
 */
function luceneapi_attachments_luceneapi_types_exclude() {
  return array_values(array_filter((array)luceneapi_node_variable_get('excluded_types')));
}

/**
 * Hook is called by search.module to add things to the search index.
 * In our case we will search content types and add any CCK type that
 * is a file type that we know how to parse and any uploaded file
 * attachments.
 */
function luceneapi_attachments_update_index() {
  $start = time();
  $cron_try = variable_get('luceneapi_attachements_cron_try', 20);
  $cron_limit = variable_get('luceneapi_attachments_cron_limit', 100);
  $cron_time_limit = variable_get('luceneapi_attachements_cron_time_limit', 15);
  $num_tried = 0;
  module_load_include('inc', 'luceneapi_attachments', 'luceneapi_attachments.admin');
  do {
    // Get all the nodes that need to be indexed
    
    // Index the nodes
    
    $num_tried += $cron_try;
  } while ($success && ($num_tried < $cron_limit) && (time() - $start < $cron_time_limit));
}

/**
 * Implementation of hook_nodeapi().
 *
 * For a delete: mark all associated attachments as removed.
 */
function luceneapi_attachments_nodeapi($node, $op) {

  switch ($op) {
    case 'update':
      module_load_include('inc', 'luceneapi_attachments', 'luceneapi_attachments.admin');
      luceneapi_attachments_add_documents($node, 'node');
      break;
    case 'delete':
      luceneapi_attachments_remove_attachments_from_index($node->nid);
      // Mark attachments for later re-deletion in case the query fails.
      db_query("UPDATE {luceneapi_attachments_files} SET removed = 1 WHERE nid = %d", $node->nid);
      break;
  }
}

/**
 * Implementation of hook_cron().
 *
 * Delete all removed attachments from the Lucene cache table.
 */
function luceneapi_attachments_cron() {
  $result = db_query("SELECT fid, nid FROM {luceneapi_attachments_files} WHERE removed = 1");
  $ids = array();
  $fids = array();
  while ($file = db_fetch_object($result)) {
    $ids[] = '/file/'.$file->fid .'-'. $file->nid;
    $fids[] = $file->fid;
  }
  if ($ids) {
    try {
      if ($index = luceneapi_index_open('luceneapi_node', $errstr)) {
        module_load_include('inc', 'luceneapi_node', 'luceneapi_node.index');
        luceneapi_document_delete($index, $node->nid, 'nid', $node, TRUE);
        cache_clear_all('luceneapi_node:', LUCENEAPI_CACHE_TABLE, TRUE);
        luceneapi_index_commit($index, TRUE);
      }
      else {
        throw new LuceneAPI_Exception($errstr);
      }
    }
    catch (Exception $e) {
      luceneapi_throw_error($e, WATCHDOG_ERROR, 'luceneapi_node');
    }

    // There was no exception, so update the table.
    db_query("DELETE FROM {luceneapi_attachments_files} WHERE fid IN (". db_placeholders($fids) .")", $fids);
  }
}

/**
 * Implementation of hook_luceneapi_query_alter().
 */
function luceneapi_attachments_luceneapi_query_alter($query, $module, $type) {
  // Fetch the extra file data on searches.
  if ($module == 'luceneapi_attachments') {
    // Add the file-based fields
    $query->add_filter .= ',ss_filemime,ss_file_node_title,ss_file_node_url';
  }
  elseif ($module == 'luceneapi_mlt') {
    // Exclude files from MLT results.
    $query->add_filter('entity', 'file', TRUE);
  }
}

/**
 * Implementation of hook_luceneapi_result_alter().
 *
 * When using the Apache Solr search module, everything is treated as a node
 * and as such values like the link and type won't be configured correctly if
 * it is a file attachement. We override such values here as needed.
 */
function luceneapi_attachments_luceneapi_result_alter(&$result, $module, $type = NULL) {

  if (isset($result['node']->ss_filemime)) {
    $nid = $result['node']->nid;
    $result['link'] = file_create_url($result['node']->path);
    $node_link = t('<em>attached to:</em> !node_link', array('!node_link' => l($result['node']->ss_file_node_title, 'node/'. $nid)));
    $icon = theme('filefield_icon', array('filemime' => $result['node']->ss_filemime));
    $file_type = t('!icon @filemime', array('@filemime' => $result['node']->ss_filemime, '!icon' => $icon));
    $result['snippet'] .= '<div>' . $file_type .' '. $node_link . '</div>';
    $result['extra'] = array();
    $result['type'] = t('File attachment');
  }
}

/**
 * For a particular node id, remove all file attachments from the solr index.
 */
function luceneapi_attachments_remove_attachments_from_index($nid) {
  try {
    if ($index = luceneapi_index_open('luceneapi_node', $errstr)) {
      module_load_include('inc', 'luceneapi_node', 'luceneapi_node.index');
      luceneapi_document_delete($index, $node->nid, 'nid', $node, TRUE);
      cache_clear_all('luceneapi_node:', LUCENEAPI_CACHE_TABLE, TRUE);
      luceneapi_index_commit($index, TRUE);
    }
    else {
      throw new LuceneAPI_Exception($errstr);
    }
  }
  catch (Exception $e) {
    luceneapi_throw_error($e, WATCHDOG_ERROR, 'luceneapi_node');
  }
}
